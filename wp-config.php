<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

@ini_set( 'upload_max_filesize' , '800M' );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lndentalchesterhill');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
//define('AUTH_KEY',         'bD]n1;h~-*AGs>D ]Qc19+(H&Cmd@-xBWbb/pr&9Jo+Sr[T|+-<VRjw*#AFbA5XN');
//define('SECURE_AUTH_KEY',  'B(]C)ghz<@_i.dg(tt5In,o6BrhdP7T)@Q!B9+ 9E4U:fR3^Wyoci3[_%BL!M~HB');
//define('LOGGED_IN_KEY',    '$<z63[+3L,:X|+O?WyCh2vbm~yn.av-gsV~#R Ex[8eC,b#AIDCh.>6PX3MjIM@>');
//define('NONCE_KEY',        '|CKt%E|~V/>W#<m=5jd)8)XU99b(W fe8c}M=@U|.OLQ4k3$+v-2ik+$02u7ds{3');
//define('AUTH_SALT',        'vf Dr0pSd+@PNvQ+H/u|[0&KMOJ--&Ywv{zQ]#L&fnp*WmJh~=eMtsl+5@qWyrMF');
//define('SECURE_AUTH_SALT', '12?(:{(,l&+{[l~Rb9H[9Rt%6jRq+<hvS{Z%z9i;!v:SuhKfZQ2,+ ryS/!32&?6');
//define('LOGGED_IN_SALT',   'e0O9q|,f5UydevL_7T3uOqt$+|2_1uJ#vJW<dbu^zV9UwPGQw}psyeo#1^4K1l(*');
//define('NONCE_SALT',       'uVLdeqSLarXxiv5[4HhE&*i[:R|@IM~3FF@MLuivxG,,5j~_Y:.N/J)}&4d+E1Z(');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
