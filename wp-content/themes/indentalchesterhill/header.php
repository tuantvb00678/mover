<!doctype html>
<html class="mouse lower modern chrome">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Open graph tags -->
    <meta property="og:site_name" content="Tiếng Anh Online Trẻ Em VIP Học 1 Kèm 1 Với Giáo Viên Bản Xứ">
    <meta property="og:title" content="Tiếng Anh Online Trẻ Em VIP Học 1 Kèm 1 Với Giáo Viên Bản Xứ">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:description" content="">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158944598-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-158944598-1');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MLWWVWB');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2594061700659150');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=2594061700659150&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- *** wordpress online css *** -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/font-awesome.css" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Baloo+Tammudu|Montserrat:100,300,400,400i,500,500i,600,600i,700,700i&display=swap&subset=telugu,vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="chrome-extension://limmgihpbambgjgfbdeannfbfcfpodfk/css/main.css">
    <!-- *** end wordpress online css *** -->

    <!-- *** wordpress head default *** -->
    <?php wp_head(); ?>
    <!-- *** end wordpress head default *** -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MLWWVWB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <!-- style pdf short code -->
    <style type="text/css">
      .center-block-horiz { margin-left: auto !important; margin-right: auto !important; }
      .set-margin-cicis-menu-to-go { margin: 20px; }
      .set-padding-cicis-menu-to-go { padding: 20px; }
      .set-border-cicis-menu-to-go { border: 5px inset #4f4f4f; }
      .set-box-shadow-cicis-menu-to-go { box-shadow: 4px 4px 14px #4f4f4f; }
      .responsive-wrapper { position: relative; height: 0; overflow: hidden; }
      .responsive-wrapper img,
      .responsive-wrapper object,
      .responsive-wrapper iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border-style: none; padding: 0; margin: 0; }
      #Iframe-Cicis-Menu-To-Go { max-width: 666.67px; max-height: 600px; overflow: hidden; }
      .responsive-wrapper-padding-bottom-90pct { padding-bottom: 90%; }
    </style>
    <!-- end style pdf short code -->
    
  </head>
  <body cz-shortcut-listen="true" <?php body_class(); ?>>
    <div id="cindi_landing">
      <!-- header fixed icon -->
      <?php the_module('header-fixed-icon'); ?>
      <div id="page">
        <!-- header menu -->
        <?php
          $menu_locations = get_nav_menu_locations();
          $menu_id = $menu_locations['header-primary'];
          $menu_items = wp_get_nav_menu_items($menu_id);
          the_module('header', array(
            'logo' => get_field('logo', 'option'),
            'menu' => $menu_items,
          )
        ); ?>

        <div class="main-container" id="main">
          <div id="content">
