<?php 
if( !empty($title) || !empty($list) ): 
  $cols = 3;
  $rowCount = 0;  
?>
<!-- start secsion box news -->
<div class="con_list_news" id="divnews">
  <div class="container">
    <h3 class="st_title"><span><?php echo $title; ?></span></h3>
    <?php if( !empty($list) ): ?>
      <div class="box_news">
        <div class="row">
          <?php 
            foreach($list as $item):
              the_module('con-new', array(
                'item' => $item
              ));
              $rowCount++;
              if($rowCount % $cols == 0 && $rowCount < count($list)) echo '</div><div class="row">';
            endforeach;
          ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
<div class="con_comments">
  <div class="container">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0"></script>
    <div class="fb-comments" data-href="https://mover.vn/" data-width="1140" data-numposts="4"></div>
  </div>
</div>

<!-- end secsion box news -->
<?php endif; ?>
