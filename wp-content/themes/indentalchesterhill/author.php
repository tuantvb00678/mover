<?php
  get_header();
  global $wp_query;
  ?>
<section class="con_list_news con_list_news--category">
    <div class="container">
        <!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>
    <div class="box_author">
         <h2><?php echo $curauth->nickname; ?></h2>
        <div class="author-box-wrap td-author-page">
            <?php echo get_avatar($part_cur_auth_obj->user_email, '96'); ?>
            <div class="desc">


                <div class="td-author-counters">
                    <span class="td-author-post-count">
                        <?php echo count_user_posts($part_cur_auth_obj->ID). ' '  . esc_html__('POSTS', 'newspaper');?>            </span>

                    <span class="td-author-comments-count">
                       <?php
                                                $comment_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) AS total FROM $wpdb->comments WHERE comment_approved = 1 AND user_id = %d", $part_cur_auth_obj->ID));

                                                echo  esc_html( $comment_count ) . ' '  . esc_html__('COMMENTS', 'newspaper');
                                            ?>           </span>

                </div>

                <div class="td-author-url"><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></div>


                <div class="td-author-social">
                </div>
            </div>
        </div>
    </div>
   

    <h2>Posts by <?php echo $curauth->nickname; ?>:</h2>
      <!-- <h3 class="st_title"><span><?php the_title(); ?></span></h3> -->
      <div class="box_author_news">
        <?php
            get_template_part('loop-archive');
        ?>
      </div>
      
    </div>
  </section>

<!-- end secsion box news -->
  <?php
  get_footer();
?>