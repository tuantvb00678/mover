<div id="header">
  <div class="con_header nav">
    <div class="container-wrap">
      <div class="row">
        <?php if( !empty($logo) ): ?>
          <div class="col-sm-2 col-xs-6">
            <div class="box-logo">
              <h1 class="logo">
                <a href="<?php echo home_url(); ?>">
                  <img src="<?php echo $logo['url']; ?>"alt="logo"/>
                </a>
              </h1>
            </div>
          </div>
        <?php endif; ?>

        <div class="col-sm-5 col-xs-6">
          <?php if( !empty($menu) ): ?>
            <div class="box-nav visible-md visible-lg visible-sm">
              <ul class="nav-left">
                <?php foreach($menu as $key=>$item): ?>
                <?php $active_class = ($key == 0) ? ' class="active" ' : ' '; ?>
                  <li><a<?php echo $active_class; ?>href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>
          <div id="no1_mommenu" class="menu-offcanvas hidden-md hidden-lg hidden-sm">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle btn2 btn-navbar offcanvas">
                <span class="sr-only"><?php _e('Toggle navigation', 'indentalchesterhill'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="overlay">
                  <?php $btn_back = THEME_URI . "/assets/images/ic_next_red.png";?>
                  <img class="btn_back" src="<?php echo $btn_back; ?>" alt="cindi back">
                </span>
              </button>

            </div>
            <div id="menu_offcanvas" class="offcanvas">
              <ul class="nav-left">
                <?php if( !empty($menu) ): ?>
                  <?php foreach($menu as $key=>$item): ?>
                    <?php $active_class = ($key == 0) ? ' class="active" ' : ' '; ?>
                    <li><a<?php echo $active_class; ?>href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
                  <?php endforeach; ?>
                <?php endif; ?>
                <li><a href="#divabout"><?php _e('Đăng Nhập', 'indentalchesterhill'); ?></a></li>
                <li><a href="#divabout"><?php _e('Trở Thành Giáo Viên', 'indentalchesterhill'); ?></a></li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-sm-5 visible-md visible-md visible-lg visible-sm">
          <div class="box-login">
            <a class="btn btn-ssm"><?php _e('Đăng Nhập', 'indentalchesterhill'); ?></a>
            <a class="btn btn-primary"><?php _e('Trở Thành Giáo Viên', 'indentalchesterhill'); ?></a>
            <div class="box_lang">
              <a class="lnk_lang dropdown-toggle lang_default" data-toggle="dropdown">&nbsp;</a>
              <ul class="dropdown-menu animated littleFadeInDown dropdown-language" role="menu">
                <li class="lag_en">
                  <?php $lang_en = THEME_URI . "/assets/images/ic_lan_en.png";?>
                  <div><img class="btn_back" src="<?php echo $lang_en; ?>" alt="cindi language">
                    <span>EN</span></div>
                </li>
                <li class="lag_vn">
                  <?php $lang_vi = THEME_URI . "/assets/images/ic_lang.png";?>
                  <div><img class="btn_back" src="<?php echo $lang_vi; ?>" alt="cindi language">
                    <span>VN</span></div>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div><!-- /#header -->
