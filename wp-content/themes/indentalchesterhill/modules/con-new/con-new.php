<div class="col-xs-12 col-sm-4">
    <div class="box_teacher">
        <p class="photo">
            <img src="<?php echo $item['image']?>" alt="<?php echo $item['title']?>" />
        </p>
        <div class="box_desc">
            <div class="box_info">
                <p class="date"><i class="fa fa-calendar-alt"></i> <?php echo $item['date']; ?></p>
                <p class="date"><i class="fa fa-comments"></i> <?php echo $item['comments_number']; ?></p>
            </div>
            <h4><a href="<?php echo $item['url']?>"><?php echo $item['title']?></a></h4>
        </div>
    </div>
</div>