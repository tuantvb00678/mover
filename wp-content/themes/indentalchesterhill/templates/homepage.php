<?php
  /**
   * Template Name: Homepage
   * Template Post Type: page
   */
  get_header();

  the_module('con-main',
    array(
      'title' => get_field('main_title'),
      'intro' => get_field('main_intro'),
      'form_id' => get_field('main_form_id')
    )
  );

  the_module('con-intro',
    array(
      'youtube_link' => get_field('intro_youtube_link'),
      'title' => get_field('intro_title'),
      'sub_title' => get_field('intro_sub_title'),
      'content' => get_field('intro_content'),
    )
  );

  $reasons = get_list_reasons(get_field('reason_list'));
  the_module('con-reason',
    array(
      'title' => get_field('reason_title'),
      'list' => $reasons,
    )
  );

  $teachers = get_list_teachers(get_field('teacher_list'));
  the_module('con-list-teacher',
    array(
      'title' => get_field('teacher_title'),
      'list' => $teachers,
      'button_label' => get_field('teacher_button_label'),
      'button_link' => get_field('teacher_button_link'),
    )
  );

  the_module('con-list-commit',
    array(
      'title' => get_field('commit_title'),
      'image' => get_field('commit_image'),
      'content' => get_field('commit_content'),
    )
  );

  $packages = get_list_packages(get_field('package_list'));
  the_module('con-pack-tuition',
    array(
      'title' => get_field('package_title'),
      'list' => $packages,
      'button_label' => get_field('package_button_label'),
      'button_url' => get_field('package_button_url'),
    )
  );

  $courses = get_list_courses(get_field('course_list'));
  the_module('con-course',
    array(
      'title' => get_field('course_title'),
      'list' => $courses,
    )
  );

  $reviewers = get_list_reviewers(get_field('reviewer_list'));
  the_module('con-reviewer',
    array(
      'title' => get_field('reviewer_title'),
      'list' => $reviewers,
    )
  );

  the_module('con-signup',
    array(
      'title' => get_field('signup_title'),
      'form_id' => get_field('signup_form_id')
    )
  );

  $news = get_list_news(get_field('news_list'));
  the_module('con-list-news',
    array(
      'title' => get_field('news_title'),
      'list' => $news,
    )
  );

  $banners = get_field('banner_list');
  the_module('con-banner',
    array(
      'list' => $banners,
    )
  );

  get_footer();
