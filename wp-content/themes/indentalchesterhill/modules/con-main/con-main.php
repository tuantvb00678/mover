<?php if ( !empty($title) || !empty($intro) ) : ?>
<div class="con_main">
  <!-- <div class="box_main"> -->
  <div class="box_slide_main">
    <div class="box_slide">
      <!-- <p class="photo"><img src="http://lndentalchesterhill:8888/wp-content/themes/indentalchesterhill/assets/images/ic_phone.png.png" alt="" /></p> -->
      <div class="container-wrap">
        <div class="box_main_info">
          <h3><?php echo $title; ?></h3>
          <div class="intro"><?php echo $intro; ?></div>
          <div class="box_btn js-form-con-main-phone">
            <!-- <a class="btn btn-number"><img src="http://lndentalchesterhill:8888/wp-content/themes/indentalchesterhill/assets/images/ic_phone.png" alt="" /> Your phone number</a> -->
            <?php if ( !empty($form_id) ) :
              echo do_shortcode( '[contact-form-7 id="'.$form_id.'"]' );
            endif; ?>
            <!-- <input class="form-control" type="number" placeholder="Your phone number" name="name" required>
            <a class="btn btn-ssm white">Enroll Now</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- </div> -->
</div>
<?php endif; ?>
