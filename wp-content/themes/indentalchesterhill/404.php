<?php
/**
 * Blank 404 file
 *
 */

  get_header();

  ?>
<div class="post-content">
  <article class="post post--article">
    <div class="con_list_news">
      <div class="container">
        <div class="st_title">
          <span><?php _e('Page Not Found', 'mover') ?></span>
        </div>
        <div class="content" style="text-align: center">
          <?php _e('Eep � this isn\'t what you were looking for.', 'mover'); ?>
        </div>
      </div>
    </div>
  </article>
</div>
<?php
while ( have_posts() ) { the_post();
  the_module('post');
}

  get_footer();
?>
