<?php
get_header();

if (is_tax('danh_muc')) { ?>
<section class="post-grid">
  <div class="post-grid__inner">
<?php } ?>

<?php
  while ( have_posts() ) { the_post();
    the_module('post');
  }
?>
<?php

if (is_category('danh_muc')) { ?> </div></section> <?php } ?>

<?php get_footer(); ?>
